<?php
/**
 * Template Name: Projects
 */ ?>
<?php
get_header();
?>

<div class="container container--narrow page-section">
    <br>
    <br>
    <div class="container text-center">
    <?php
    $wp_query = new WP_Query(array('posts_per_page'=>-1));
    while ($wp_query->have_posts()) : $wp_query->the_post();?>
    <h1><?php the_title();?></h1>
    <p><?php the_content(); ?></p>
    <?php endwhile;
    ?>
    </div>
<?php

get_footer();

?>

