
<?php
/**
 * Template Name: index
 */ ?>

<?php
get_header();

 ?>

<div class="container container--narrow page-section">
<?php
   {
     ?>
      <div class="container text-center">
       <br>
       <br>
    <div class="post-item">

        <h1>Welcome</h1>





        <br>


      <div class="generic-content">

          <p>Hi, my name is Cameron Simmons. I am an open source developer.</p>

          <p>I’ve used a number of technologies such as C++, Java, Javascript, Python, PHP, TypeScript, R, HTML, and CSS.</p>

          <p>In my applications, I’ve used MariaDB, MySQL, PostgreSQL, and NoSQL for databases.</p>

          <p>For project examples, visit the code section <a href="<?php echo site_url('/projects') ?>">here</a>.</p>


      </div>

    </div>
      </div>


  <?php }

?>
</div>

<?php get_footer();

?>