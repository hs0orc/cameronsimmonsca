<head>
    <?php wp_head(); ?>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js">
    </script>

    <link rel="stylesheet" href="style.css">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Antic+Didone&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">

</head>

<nav class="navbar navbar-expand-md navbar-light bg-faded">
    <ul class="navbar-nav mx-auto">
        <li class="nav-item active text-center">

            <a class="nav-link" href="<?php echo site_url('/') ?>"> <i class="fas fa-home fa-3x" style="color:#003366;"></i></a>

        </li>

        <li class="nav-item active text-center">

            <a class="nav-link" href="<?php echo site_url('/projects') ?>"> <i class="fas fa-file-code fa-3x" style="color:#003366;"></i></a>

        </li>



        <li class="nav-item text-center">
            <a class="nav-link" href="<?php echo site_url('/contact') ?>"> <i class="fas fa-envelope-square fa-3x" style="color:#003366;"></i></a>

        </li>

        <!--<li class="nav-item dropdown text-center">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown link
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
            </div>

        </li>-->
    </ul>
</nav>

<!-- <h1>Testing h1</h1>
<p>Testing p</p> -->